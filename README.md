# Outputs

_The following outputs are generated automatically when you finish creating your Collection by running gitlab's CI pipeline._

- ### [Collection Documentation](https://gitlab.com/our-sci/software/json_schema_distribution/-/jobs/artifacts/main/browse/output/schemata?job=copy_schemas)

- ### [Schema Validation - NPM package](https://www.npmjs.com/package/json_schema_experiments)
- ### [Schema Validation - Browser Version](https://cdn.jsdelivr.net/npm/json_schema_experiments/dist/module/farmos_schemata_validator.js)


# Description

  This repo centralizes several technical experiments OurSci and OpenTeam are performing around JSON schemas. Our aim is to get the most precise possible schemas to ensure the Ag Data Wallet is kept up to its standards and that the tools validating these standards are simple enough to use to make it compelling for the users to have the tools inside their procedures.
  
# Objectives

* Facilitate the deployment of schema validation tools to users at any technical level, with maximum precission and the least amount of scaffolding possible.
* Create documentation helping users of any level get productive with schema creation and validation.
* Review and improve the precission of existing schemas.
* Create a pathway to schematize and validate conventions. 
* Provide a way to centralize the publication of schemas, conventions and validation code.

# How to use it

## Configure CI environment variables

* You will neeed to provide an aggregator key with access to your target model farm, with the name FARMOS_KEY.
* If you want to publish a validator package, you need to publish your *npm toke* in the `.env` file as **NPM_AUTH_TOKEN**.

## Configure the sources for FarmOS schemas and conventions

 Both should be parametrized in the `.env` file.
 It only has two mandatory variables: 
 
 * `FARM_DOMAIN`: the url of a valid FarmOS instance known to be up to date with the expected data standards. A *model* instance.
 * `CONVENTIONS_GIT_SOURCE`: The git url of a git repo hosting a set of `conventions` with the format we expect here. It is currently NOT parametrizable, we will get the schemas from a local folder for now.

A third, non mandatory variable will be required if you want to publish your validators as an npm package:

 * `PUBLISHED_PACKAGE_NAME`

Publishing will also require you to configure a **private CI variable** with your NPM credentials for a valid target repo. 

Currently, **we are NOT accepting any source but local conventions**, but this will be changed soon. The structure in the repo will be the same we are using here.
 
### Structure of the `conventions` folder
 
* For each convention, a folder with the convention name is expected.
* Inside the folder, one subdirectory is `examples`, in which two further paths should be `correct` and `incorrect`. These will be used to test is the schema is accepting the entitites we want it to accept and rejecting the entities we want it to reject. We will know if a schema is working properly if it **accepts the corrent examples**  and **rejects the incorrect examples**.
* In the root path, a file named `schema.json` should have a schema describing our convention. Typically, the convention will have several fields and **each field should have atype/bundle from the parametrized farm**, with a name unique to the schema.

## What the repo will do for you

* It will download and tidy (de drupalized) all schemas from the selected farm, using FarmOS.js (the script doing that is `./scripts/getAllSchemas.js`).
* It will test each provided convention against each *correct* and *incorrect* and inform if the results were corresponding to what was expected (see `./test/conventions_against_examples.test.js`).
* If you like all the changes you've done, your tests are passing and you want to merge into `main`, it will publish the new version of the schemas as artifacts and it will publish validation code for all your schemas and conventions in a uniquely named package (`./scripts/compileAllValidators.js` and `./scripts/transpilation.js` to get a browser compatible version of the code, also publised in the library). 
 
### What it should do in the near future

* Offer an easy to use `gitpods` interface to allow you to work with the needed libraries with 0 configuration and get acquainted with the whole process.

# How to define new Conventions

We created a library which helps build a convention in the proposed style using high level commands and facilitating testing against examples, documenting, etc. Its functionality is contained in the `convention_builder` module. See documentation [here](https://our-sci.gitlab.io/software/json_schema_distribution/index).

# Other Contents of the Repo

## Experiments

* Experiments are documented in a further document inside the **examples** folder.

  We explored several technical pathways we need to achieve the aims we stated above. Besides, we documented our explorations as much as possible, because we believe there will be a need to make JSON schema usage easy and immediate for developers of this spaces and the tool is simple enough, just lacking in documentation around the kind of usage we envision. 
  
  
## Centralized Publication of Schemas and Conventions

  This repo incorporates a full CI/CD workflow that retrieves schemas from a model farm (chosen by the user with a parameter) and publishes each schema in a de drupalized form into folders structured by type and bundle. Besides it compiles autonomous schema validator functions using AJV, allowing to check if a file adheres to a schema in an as efortless way as possible.
  The schemas and validators are published into NPM to allow easy retrieval both in backend and frontend applications (via a node connected CDN).
  Besides we will offer a model Express endpoint to allow to share the schemas via any server with an easy model query using the type and bundle as parameters. 
  
### How is this done

A CI/CD pipeline publishes both the schemas as static files and the validators as an NPM library. Schemas are store inside the `output/schemata` archive structure. 
  
## Improving existing schemas

  * It is important to offer a **de drupalized** version of the schema, which is the one effectively working in the validation of a payload sent to farmOS, works with JSON schema validation tools and is also easier to read. The de drupalization is performed by `FarmOS.js`.
  * We also want to **make some currently implicit constraints explicit**. An example is the `measure` field in `quantity` shemas, which is currently listed as `string` but is really constrained to a fixed list of possible values.
  
  
### How we plan to do this

We will store **overlays** next to the de drupalized schemas, containing the known **implicit** conventions. These will be applied when building the validator code.
  
  
## Development of a Conventions Schema Format

  Currently, the FarmOS community and the open Ag Wallet community are working on the concept of `conventions` as constructs involving many `entities` and several conditions around how they are filled and interconected.
  We believe being able to express these conventions around a `schema` would be fairly valuable as even filling one entity can be demanding when working out of the railguards of the FarmOS web interface. Also, having a strong validation tool would allow `conventions` to be more precise, strict and specific without demanding more effort from users.


### How is this done

  We already have propositions of functional **convention schemas**  between the examples, as well as validators for these schemas.

# Documentation

- [Latest version of detailed code documentation.](https://our-sci.gitlab.io/software/json_schema_distribution/index)

