## tillage_log

* **Type: log--activity** 
  * Must be related to a taxonomy_term--log_category named *tillage* and be related to an *asset--land*.
  * Should have quantity--standard--stir, quantity--standard--residue, quantity--standard--tillage_depth.
  * May have other taxonomy_term--log_category.
  * Originally hosted in [link](https://gitlab.com/OpenTEAMAg/ag-data-wallet/openteam-convention/-/blob/main/descriptions/log--activity--tillage.md)

## stir_quantity

* **Type: quantity--standard** 
  * Must be labelled as *stir* and it's measure type is *ratio*.
  * See documentation ADD LINK to get the standard specification for this ratio.

## depth_quantity

* **Type: quantity--standard** 
  * Must be labelled as *depth* and it's measure type is *length*.

