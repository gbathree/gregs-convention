const builder = require(`../src/convention_builder`);
const { randomUUID } = require('crypto');

// let irrigationActivity = new SchemaOverlay({ typeAndBundle: "log--activity" , name:"irrigation", validExamples: [], erroredExamples: [] });

let validExample = { attributes:{ name: "tillage", status:"done", data: "plow" } };
let errorExample_1 = { attributes:{ name: "tillage_error", data: "activity" } };
let errorExample_2 = { attributes:{ name: "tillage_error", status:2, data: "wrong" } };

let tillageLog = new builder.SchemaOverlay({
    typeAndBundle: "log--activity",
    name: "tillage",
    validExamples: [validExample],
    erroredExamples: [ errorExample_1, errorExample_2 ]
});

tillageLog.setMainDescription("Must be related to a taxonomy_term--log_category named *tillage* and be related to an *asset--land*. Should have quantity--standard--stir, quantity--standard--residue, quantity--standard--tillage_depth. May have other taxonomy_term--log_category. Originally hosted in [link](https://gitlab.com/OpenTEAMAg/ag-data-wallet/openteam-convention/-/blob/main/descriptions/log--activity--tillage.md)");
tillageLog.setConstant( {
    attribute:"name",
    value:"tillage"
} );


let stirQuantity = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "STIR"
});
stirQuantity.setMainDescription("Must be labelled as *stir* and it's measure type is *ratio*. See documentation ADD LINK to get the standard specification for this ratio.");
stirQuantity.setConstant( {
    attribute:"label",
    value:"STIR",
} );
stirQuantity.setConstant( {
    attribute:"measure",
    value:"ratio",
} );

let depthQuantity = new builder.SchemaOverlay({
    typeAndBundle: "quantity--standard",
    name: "tillage_depth"
});
depthQuantity.setMainDescription("Must be labelled as *depth* and it's measure type is *length*.");
depthQuantity.setConstant( {
    attribute:"label",
    value:"depth",
} );
depthQuantity.setConstant( {
    attribute:"measure",
    value:"length",
} );

let logUUID = randomUUID();
let stirUUID = randomUUID();
let depthUUID = randomUUID();

let example = {
    // each entity will be an attribute in the convention, with a reference to a known entity type.
    // we need more precission, for example constants.
    tillage_log: {
        attributes: {
            status:'done',
            name: "tillage"
        },
        relationships: {
            quantity: [
                {
                    type: "quantity--standard",
                    id: stirUUID
                },
                {
                    type: "quantity--standard",
                    id: depthUUID
                }
            ],
            location: [
                { type: "asset--land",
                  id: randomUUID()
                }
            ]
        },
        id: logUUID
    },
    stir_quantity: {
        attributes: {label: "STIR"},
        id: stirUUID
    },
    depth_quantity: {
        attributes: {
            label:"depth"
        },
        id: depthUUID
    }
};

let exampleError = {
    // each entity will be an attribute in the convention, with a reference to a known entity type.
    // we need more precission, for example constants.
    activity_log: {
        id: logUUID,
        attributes: {
            status:'done',
            name: "tillage log"
        },
        relationships: {
            quantity: [ {
                type: "quantity--standard",
                id: '695e1d38-a1d6-4ed7-8d9d-c0b818a2cbfe'
            } ]
        },
        relationships: {
            quantity: [ {
                type: "quantity--standard",
                id: 'b71f7bb3-da43-40b1-8dc3-965e5f912763'
            } ]
        }
    },
    stir_quantity: {
        id: logUUID,
        attributes: {label: "stir"}
    },
};

let tillageConvention = new builder.ConventionSchema({
    title: "tillage_event",
    version:"0.0.1",
    schemaName:"log--activity--tillage_script",
    repoURL:"www.gitlabrepo.com/version/farmos_conventions",
    description:"A tillage log encompasses all information we can gather about a tillage operation.",
    validExamples: [ example ],
    erroredExamples: [exampleError]
});

tillageConvention.addAttribute( { schemaOverlayObject:tillageLog, attributeName: "tillage_log", required: true } );
tillageConvention.addAttribute( { schemaOverlayObject:stirQuantity, attributeName: "stir_quantity", required: false } );
tillageConvention.addAttribute( { schemaOverlayObject:depthQuantity, attributeName: "depth_quantity", required: false } );
tillageConvention.addRelationship( { containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"stir_quantity" , required: false } );
tillageConvention.addRelationship( { containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"depth_quantity" , required: false } );

let conventionTest = tillageConvention.testExamples();
let storageOperation = tillageConvention.store();
