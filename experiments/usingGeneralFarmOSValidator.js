const validations = require("../output/validators/allFarmOSSchemas");
// const { asset__land } = require("../output/validators/allFarmOSSchemas");

let example = {
    type: "asset--land",
    attributes: {
        name: "Testing Land",
        status: "Active",
        land_type: "property"
    }
};

let validation = validations["asset--land"](example);


let exampleError = {
    type: "asset--land",
    attributes: {
        name: "Testing Land",
        status: false,
        land_type: "property"
    }
};

let validationError = validations["asset--land"](exampleError);
validations["asset--land"].errors;
