const builder = require("../src/convention_builder.js");
const fs = require("fs");
const { randomUUID } = require('crypto');

fs.mkdirSync( `${__dirname}/conventions`, { recursive: true }, console.error );
fs.mkdirSync( `${__dirname}/conventions/overlays`, { recursive: true }, console.error );

describe( "Testing the SchemaOverlay class, used to define specific versions of entity types.", () => {
    test("A schema is acquired.", () => {
        const tillageLogActivity = new builder.SchemaOverlay({ typeAndBundle:"log--activity", name:"tillage" });
        expect(tillageLogActivity.baseSchema.title).toBe("Activity log");
    });

    test("An error is generated for invalid baseSchema names.", () => {
        expect( () => new builder.SchemaOverlay({ typeAndBundle:"log/activity", name:"tillage" })).toThrow(`No schema for log/activity was found. This parameter should be a type--bundle pair, as farmOS mandates (examples are "log--lab_test" and "asset--land").`);
    });

    test("An error is generated for non existent, valid baseSchema names.", () => {
        expect( () => new builder.SchemaOverlay({ typeAndBundle:"log--non_existent_activity", name:"tillage" })).toThrow(`No schema for log--non_existent_activity was found. This might be due to the schema not existing in the farm, or the current "output/schemata" folder structure being out of date. You can update it using the "getAllSchemas.js" script, in the script folder.`);
    });

    test("A constant is applied.", () => {
        const tillageLogActivity = new builder.SchemaOverlay({ typeAndBundle:"log--activity", name:"tillage" });
        tillageLogActivity.setConstant( { attribute:"name", value:"tillage" } );
        expect(tillageLogActivity.schema.properties.attributes.properties.name.const).toBe("tillage");
    });


    test("A description is added when a constant is set.", () => {
        const tillageLogActivity = new builder.SchemaOverlay({ typeAndBundle:"log--activity", name:"tillage" });
        tillageLogActivity.setConstant( { attribute:"name", value:"tillage", description:"Test description." } );
        expect(tillageLogActivity.schema.properties.attributes.properties.name.description).toBe("Test description.");
    });

    test("An numeration is applied.", () => {
        const tillageLogActivity = new builder.SchemaOverlay({ typeAndBundle:"log--activity", name:"tillage" });
        tillageLogActivity.setEnum( { attribute:"name", valuesArray:[ "tillage", "tillage_log", "plow" ], description:"Accepting all synonims." } );
        expect(tillageLogActivity.schema.properties.attributes.properties.name.enum).toStrictEqual([ "tillage", "tillage_log", "plow" ]);
        expect(tillageLogActivity.schema.properties.attributes.properties.name.description).toBe("Accepting all synonims.");
    });

    test("Setting non existing attributes triggers an error.", () => {
        const tillageLogActivity = new builder.SchemaOverlay({ typeAndBundle:"log--activity", name:"tillage" });
        expect( () => tillageLogActivity.setConstant( { attribute:"non_existing_attr", value:"tillage", description:"Test description." } ) ).toThrow("Attribute non_existing_attr is unknown to the schema log--activity.");
    });

    test("Overwriting specifications triggers an error.", () => {
        const tillageLogActivity = new builder.SchemaOverlay({ typeAndBundle:"log--activity", name:"tillage" });
        tillageLogActivity.setConstant( { attribute:"name", value:"1_tillage", description:"Test description." } );
        expect( () => tillageLogActivity.setConstant( { attribute:"name", value:"2_tillage", description:"Test description." } ) ).toThrow("Attribute name has already been modified for this overlay, you are overwriting previously declared changes. Instead, make only one operation encompassing all the changes together.");
    });

    test("testExamples method is able to detect success", () => {
        let validExample = { attributes:{ name: "tillage", status:"done", data: "plow" } };
        let errorExample_1 = { attributes:{ name: "tillage_error", data: "activity" } };
        let errorExample_2 = { attributes:{ name: "tillage_error", status:2, data: "wrong" } };
        const tillageLogActivity = new builder.SchemaOverlay(
            {
                typeAndBundle:"log--activity",
                name:"tillage",
                validExamples: [validExample],
                erroredExamples: [ errorExample_1, errorExample_2 ]
            });
        tillageLogActivity.setConstant( { attribute:"name", value:"tillage", description:"Test description." } );
        tillageLogActivity.setEnum( { attribute:"data", valuesArray:[ "tillage", "plow" ], description:"Test description." } );

        let testResult = tillageLogActivity.testExamples();

        expect(testResult.success).toBe(true);
    });

    test("testExamples method is able to detect failure and deliver information.", () => {
        let errorExample_1 = { attributes:{ name: "tillage_error", data:"wrong" } };
        let errorExample_2 = { attributes:{ name: "tillage_error", status:2 } };
        const tillageLogActivity = new builder.SchemaOverlay(
            {
                typeAndBundle:"log--activity",
                name:"tillage", validExamples: [ errorExample_1 ],
                erroredExamples: [ errorExample_2 ]
            });
        tillageLogActivity.setConstant( { attribute:"name", value:"tillage", description:"Test description." } );
        tillageLogActivity.setEnum( { attribute:"data", valuesArray:[ "tillage", "plow" ], description:"Test description." } );

        let testResult = tillageLogActivity.testExamples();

        // property mandatory in original schema
        let error1 = "must have required property 'status'";
        // property set by set constant
        let error2 = 'must be equal to constant';
        // property set by set enum
        let error3 = 'must be equal to one of the allowed values';

        let failedExamples = testResult.failedExamples.flatMap( d => d.errors );

        // console.log("failed examples");
        // console.log(failedExamples);
        expect(testResult.success).toBe(false);
        expect(failedExamples.map(ex => ex.message)).toStrictEqual( [ error1, error2, error3 ] );
    });

    test("Storage works.", () => {
        let validExample = { attributes:{ name: "tillage", status:"done", data: "plow" } };
        let errorExample_1 = { attributes:{ name: "tillage_error", data: "activity" } };
        let errorExample_2 = { attributes:{ name: "tillage_error", status:2, data: "wrong" } };
        const tillageLogActivity = new builder.SchemaOverlay(
            {
                typeAndBundle:"log--activity",
                name:"tillage",
                validExamples: [validExample],
                erroredExamples: [ errorExample_1, errorExample_2 ],
                storagePath: `${__dirname}/conventions/overlays`

            });
        tillageLogActivity.setConstant( { attribute:"name", value:"tillage", description:"Test description." } );
        tillageLogActivity.setEnum( { attribute:"data", valuesArray:[ "tillage", "plow" ], description:"Test description." } );

        let testResult = tillageLogActivity.testExamples();
        let storageStats = tillageLogActivity.store();
        let correctExamples = fs.readdirSync( `${storageStats.path}/examples/correct`, (error, files) => {
            return files;
        } );
        let incorrectExamples  = fs.readdirSync( `${storageStats.path}/examples/incorrect`, (error, files) => {
            return files;
        } );

        fs.rmSync(`${__dirname}/conventions/overlays/log--activity--tillage`, {recursive:true, force:true});
        expect(storageStats.valid).toBe(true);
        expect(correctExamples.length).toBe(tillageLogActivity.validExamples.length);
        expect(incorrectExamples.length).toBe(tillageLogActivity.erroredExamples.length);
        expect(storageStats.path).toBe(`${__dirname}/conventions/overlays/log--activity--tillage`);
    });

} );

describe( "Testing the main convention builder.", () => {

    let tillageLog = new builder.SchemaOverlay({
        typeAndBundle: "log--activity",
        name: "tillage",
        storagePath: `${__dirname}/conventions/overlays`
    });
    tillageLog.setMainDescription("Must be related to a taxonomy_term--log_category named *tillage* and be related to an *asset--land*. Should have quantity--standard--stir, quantity--standard--residue, quantity--standard--tillage_depth. May have other taxonomy_term--log_category. Originally hosted in [link](https://gitlab.com/OpenTEAMAg/ag-data-wallet/openteam-convention/-/blob/main/descriptions/log--activity--tillage.md)");
    tillageLog.setConstant( {
        attribute:"name",
        value:"tillage"
    } );
    let stirQuantity = new builder.SchemaOverlay({
        typeAndBundle: "quantity--standard",
        name: "STIR",
        storagePath: `${__dirname}/conventions/overlays`
    });
    stirQuantity.setMainDescription("Must be labelled as *stir* and it's measure type is *ratio*. See documentation ADD LINK to get the standard specification for this ratio.");
    stirQuantity.setConstant( {
        attribute:"label",
        value:"STIR",
    } );
    stirQuantity.setConstant( {
        attribute:"measure",
        value:"ration",
    } );
    let depthQuantity = new builder.SchemaOverlay({
        typeAndBundle: "quantity--standard",
        name: "tillage_depth",
        storagePath: `${__dirname}/conventions/overlays`
    });
    depthQuantity.setMainDescription("Must be labelled as *depth* and it's measure type is *length*.");
    depthQuantity.setConstant( {
        attribute:"label",
        value:"depth",
    } );
    depthQuantity.setConstant( {
        attribute:"measure",
        value:"length",
    } );

    let logUUID = randomUUID();
    let stirUUID = randomUUID();
    let depthUUID = randomUUID();

    let example = {
        // each entity will be an attribute in the convention, with a reference to a known entity type.
        // we need more precission, for example constants.
        tillage_log: {
            attributes: {
                status:'done',
                name: "tillage"
            },
            relationships: {
                quantity: [
                    {
                        type: "quantity--standard",
                        id: stirUUID
                    },
                    {
                        type: "quantity--standard",
                        id: depthUUID
                    }
                ],
                location: [
                    { type: "asset--land",
                      id: randomUUID()
                    }
                ]
            },
            id: logUUID
        },
        stir_quantity: {
            attributes: {label: "STIR"},
            id: stirUUID
        },
        depth_quantity: {
            attributes: {
                label:"depth"
            },
            id: depthUUID
        }
    };
    let exampleError = {
        // each entity will be an attribute in the convention, with a reference to a known entity type.
        // we need more precission, for example constants.
        activity_log: {
            id: logUUID,
            attributes: {
                status:'done',
                name: "tillage log"
            },
            relationships: {
                quantity: [ {
                    type: "quantity--standard",
                    id: '695e1d38-a1d6-4ed7-8d9d-c0b818a2cbfe'
                } ]
            },
            relationships: {
                quantity: [ {
                    type: "quantity--standard",
                    id: 'b71f7bb3-da43-40b1-8dc3-965e5f912763'
                } ]
            }
        },
        stir_quantity: {
            id: logUUID,
            attributes: {label: "stir"}
        },
    };

    let tillageConvention = new builder.ConventionSchema({
        title: "tillage_event",
        schemaName: "log--activity--tillage_log_test",
        version:"0.0.1",
        repoURL:"www.gitlabrepo.com/version/farmos_conventions/",
        description:"A tillage log encompasses all information we can gather about a tillage operation.",
        validExamples: [ example ],
        erroredExamples: [exampleError],
        storagePath: `${__dirname}/conventions`
    });

    tillageConvention.addAttribute( { schemaOverlayObject:tillageLog, attributeName: "tillage_log", required: true } );
    tillageConvention.addAttribute( { schemaOverlayObject:stirQuantity, attributeName: "stir_quantity", required: false } );
    tillageConvention.addAttribute( { schemaOverlayObject:depthQuantity, attributeName: "depth_quantity", required: false } );
    tillageConvention.addRelationship( { containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"stir_quantity" , required: false } );
    tillageConvention.addRelationship( { containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"depth_quantity" , required: false } );
    // console.log("schema");
    // console.log(tillageConvention.schema.properties.tillage_log.properties.relationships.properties.quantity.prefixItems.map( d => d.properties.type.const ));


    test("A convention gets built.", () => {
        expect(tillageConvention.schema.properties.tillage_log.properties.relationships.properties.quantity.prefixItems.length ).toBe(2);
        let testResult = tillageConvention.testExamples();
        // console.log(testResult);
        let failedExamples = testResult.failedExamples.flatMap( d => d.errors );

        // console.log("failed examples");
        // console.log(failedExamples);
        // console.log(failedExamples.map(d => d.params.allowedValue));
        expect(testResult.success).toBe(true);
    });

    test("A convention gets written.", () => {
        let storeStats = tillageConvention.store();
        console.log(storeStats);
        let correctExamples = fs.readdirSync( `${storeStats.path}/examples/correct`, (error, files) => {
            return files;
        } );
        let incorrectExamples  = fs.readdirSync( `${storeStats.path}/examples/incorrect`, (error, files) => {
            return files;
        } );
        console.log(correctExamples);
        console.log(incorrectExamples);
        fs.rmSync(`${__dirname}/conventions/log--activity--tillage_log_test`, {recursive:true, force:true});
        console.log(storeStats);
        expect(storeStats.valid).toBe(true);
        expect(correctExamples.length).toBe(tillageConvention.validExamples.length);
        expect(incorrectExamples.length).toBe(tillageConvention.erroredExamples.length);
        expect(storeStats.path).toBe(`${__dirname}/conventions/log--activity--tillage_log_test`);
    });

    test("Wrong relationship fields throw an informative error.", () => {
        expect( () => tillageConvention.addRelationship({ containerEntity:"tillage_log" , relationName:"amount" , mentionedEntity:"stir_quantity" , required: false }) ).toThrow("The provided relationship field, 'amount', doesn't seem to be a valid field in the schema for 'log--activity'. Available fields are: file, image, location, asset, category, quantity, owner.");
        expect( () => tillageConvention.addRelationship({ containerEntity:"tillage" , relationName:"quantity" , mentionedEntity:"stir_quantity" , required: false }) ).toThrow("Container entity 'tillage' has not been added as an attribute, please add a SchemaOverlay describing it before using it in a relationship.");
        expect( () => tillageConvention.addRelationship({ containerEntity:"tillage_log" , relationName:"quantity" , mentionedEntity:"stir" , required: false }) ).toThrow("Mentioned entity 'stir' has not been added as an attribute, please add a SchemaOverlay describing it before using it in a relationship.");
    });
} );
